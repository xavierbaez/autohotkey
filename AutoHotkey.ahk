; IMPORTANT INFO ABOUT GETTING STARTED: Lines that start with a
; semicolon, such as this one, are comments.  They are not executed.

; This script has a special filename and path because it is automatically
; launched when you run the program directly.  Also, any text file whose
; name ends in .ahk is associated with the program, which means that it
; can be launched simply by double-clicking it.  You can have as many .ahk
; files as you want, located in any folder.  You can also run more than
; one ahk file simultaneously and each will get its own tray icon.

; SAMPLE HOTKEYS: Below are two sample hotkeys.  The first is Win+Z and it
; launches a web site in the default browser.  The second is Control+Alt+N
; and it launches a new Notepad window (or activates an existing one).  To
; try out these hotkeys, run AutoHotkey again, which will load this file.

;#  - Win
;!  - Alt
;^  - Control
;+  - Shift

#`::
    KeyWait, ``               ; wait for ` to be released
    KeyWait, ``, D T0.7       ; and pressed again within 0.7 seconds
    if ErrorLevel  {          ; timed-out (only a single press)
        Run "C:\Windows\System32\cmd.exe"
   } Else
        Send #;
Return

^!n::  ; Ctrl+Alt+N
if WinExist("Untitled - Notepad")
    WinActivate
else
    Run Notepad
return

; XB STARTS

; dwm
#+c:: Send !{f4} ; Close Applications like DWM.
;Win+E
#e::Run xyplorer
;Win+Enter Git Bash
#Enter::Run "C:\Program Files\Git\git-bash.exe" --cd-to-home

; Browsers
;Win+B
#b::Run brave
;Win+C
#c::Run chrome
;Win+F
#f::Run firefox
;Win+R
#r::Run "C:\Program Files\Rambox\Rambox.exe"
;Win+v
#v::Run "C:\Users\Xavier\AppData\Local\Vivaldi\Application\vivaldi.exe"

;Development
;Win+Ctrl+N
#^n::Run "C:\Users\Xavier\AppData\Local\Programs\Evernote\evernote.exe"
;Win+Ctrl+S
#^s::Run "C:\Program Files\Development\Sublime Text\subl.exe"

; Jetbrains
;Win+Ctrl+C
#^c::Run "C:\Development\Jetbrains Toolbox\apps\CLion\ch-0\221.5591.52\bin\clion64.exe"
;Win+Ctrl+I
#^i::Run "C:\Development\Jetbrains Toolbox\apps\IDEA-U\ch-0\221.5591.52\bin\idea64.exe"
;Win+Ctrl+P
#^p::Run "C:\Development\Jetbrains Toolbox\apps\PhpStorm\ch-0\221.5591.58\bin\phpstorm64.exe"
;Win+Ctrl+R
#^r::Run "C:\Development\Jetbrains Toolbox\apps\Rider\ch-0\221.5591.20\bin\rider64.exe"
;Win+Ctrl+W
#^w::Run "C:\Development\Jetbrains Toolbox\apps\WebStorm\ch-0\221.5591.52\bin\webstorm64.exe"
;Win+Ctrl+Y
#^y::Run "C:\Development\Jetbrains Toolbox\apps\PyCharm-P\ch-0\221.5591.52\bin\pycharm64.exe"

; Volume Controls
;Ctrl+Alt+Up
^!up::Send {Volume_Up}
;Ctrl+Alt+Down
^!down::Send {Volume_Down}
;Ctrl+Alt+m
^!m::Send {Volume_Mute}

; Note: From now on whenever you run AutoHotkey directly, this script
; will be loaded.  So feel free to customize it to suit your needs.

; Please read the QUICK-START TUTORIAL near the top of the help file.
; It explains how to perform common automation tasks such as sending
; keystrokes and mouse clicks.  It also explains more about hotkeys.

;#IfWinActive, Mozilla Firefox
;	Volume_Up::Send,^{WheelUp}
;	Volume_Down::Send,^{WheelDown}

#IfWinActive StarCraft II
	capslock::suspend
	w::up
	s::down
	a::left
	d::right
	q::insert
	e::delete
	Up::w
	Down::s
	Left::a
	Right::d

#IfWinActive Sid Meier's Civilization V
	capslock::suspend
	w::up
	s::down
	a::left
	d::right
	q::space
	e::enter
	space::enter
	Shift::space

#IfWinActive Untitled - Notepad
	capslock::suspend

#^+^Q::
    KeyWait, Q
    KeyWait, Q, D T0.1
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return

#^+^W::
    KeyWait, W
    KeyWait, W, D T0.2
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return

#^+^E::
    KeyWait, E
    KeyWait, E, D T0.3
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return

#^+^R::
    KeyWait, R
    KeyWait, R, D T0.4
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return

#^+^T::
    KeyWait, T
    KeyWait, T, D T0.5
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return


#^+^Y::
    KeyWait, Y
    KeyWait, Y, D T0.6
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return

#^+^U::
    KeyWait, U
    KeyWait, U, D T0.6
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return

#^+^I::
    KeyWait, I
    KeyWait, I, D T0.8
    if ErrorLevel  {
        MsgBox One press
   } Else
        MsgBox Double press
Return


#^+^O::
	KeyWait,O
	KeyWait,O,D T0.9
	If ErrorLevel {
		MsgBox One press
	} Else
		MsgBox Double press
Return

#^+^P::
	KeyWait,P
	KeyWait,P,D T1.0
	If ErrorLevel {
		MsgBox One press
	} Else
		MsgBox Double press
Return

;#ah::Run https://www.autohotkey.com  ; Win+Z